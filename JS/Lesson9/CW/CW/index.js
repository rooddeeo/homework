//Стили
const stopwatch = document.querySelector('#num');
stopwatch.style.display = 'flex'
stopwatch.style.textAlign = 'center'
stopwatch.style.justifyContent = 'center'
stopwatch.style.fontSize = '30px'
stopwatch.style.border = 'solid, black 1px'
stopwatch.style.borderRadius = '5px'
stopwatch.style.height = '35px'
stopwatch.style.backgroundColor = 'silver';
stopwatch.style.width = '155px'

const stopwatch1 = document.querySelector('.stopwatch');
stopwatch1.style.display = 'flex'
stopwatch1.style.alignItems = 'center'
stopwatch1.style.justifyContent = 'center'
stopwatch1.style.flexDirection = 'column'

const stopwatch2 = document.querySelector('.button');
stopwatch2.style.marginTop = '15px';

//Секундомер

let miliSec = '00',
sec = '00',
min = '00',
start

const startFun = () => {
  start = setInterval(count, 10)
};
const count = () => {
  miliSec++
  if (miliSec < 10) {
    miliSec = '0' + miliSec
} else {miliSec}
    if(miliSec === 99){
    sec++
    if (sec < 10) {
      sec = '0' + sec
    } else {sec}
    miliSec = 00
    if(sec === 59){
      min++
      if (min < 10) {
        min = '0' + min
      } else {min}
      sec = 00
      
    }
  };
  document.querySelector('#num').innerHTML = `${min}:${sec}:${miliSec}`;
};

const clearFun = () => {
  const num = document.querySelector("#num");
  num.innerHTML = "00:00:00";

};
const stopFun = () => {
  clearInterval(start)
};

document.querySelector('#start').onclick = startFun;
document.querySelector('#stop').onclick = stopFun;
document.querySelector('#clear').onclick = clearFun;